package repo

import (
	pb "gitlab.com/grpc-first/user_service/genproto/user"
)

type UserStorageI interface {
	Create(*pb.User) (*pb.User, error)
	GetUsersById(*pb.Ids) (*pb.GetUsers, error)
	UpdateUser(*pb.User) (*pb.User, error)
	DeletUser(*pb.Ids) error
	//buy
	BuyProduct(*pb.PutProductToDB) error
	GetProperty(OwnerId int64) ([]*pb.PutProductToDB, error)
	SetProfileImage(req *pb.SetProfileImageReq) (*pb.GetProfileImageRes, error)
	CheckField(req *pb.CheckFieldReq) (*pb.CheckFieldRes, error)
}
