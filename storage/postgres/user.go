package postgres

import (
	"fmt"

	pb "gitlab.com/grpc-first/user_service/genproto/user"

	"github.com/jmoiron/sqlx"
)

type UserRepo struct {
	Db *sqlx.DB
}

func NewUserRepo(db *sqlx.DB) *UserRepo {
	return &UserRepo{Db: db}
}

func (r *UserRepo) Create(user *pb.User) (*pb.User, error) {
	err := r.Db.QueryRow(`INSERT INTO 
	users(first_name, last_name, email, username, password)  
	values($1, $2, $3, $4, $5) returning id`,
		user.FirstName,
		user.LastName,
		user.Email,
		user.Usenrame,
		user.Password,
	).Scan(
		&user.Id,
	)
	return user, err
}

func (r *UserRepo) GetUsersById(ids *pb.Ids) (*pb.GetUsers, error) {
	response := &pb.GetUsers{}
	for _, id := range ids.Ids {
		tempUser := &pb.User{}
		err := r.Db.QueryRow(`SELECT 
		id, 
		first_name, 
		last_name, 
		email, 
		username, 
		password 
		from users where id = $1`, id).Scan(
			&tempUser.Id,
			&tempUser.FirstName,
			&tempUser.LastName,
			&tempUser.Email,
			&tempUser.Usenrame,
			&tempUser.Password,
		)
		if err != nil {
			return &pb.GetUsers{}, err
		}
		response.Users = append(response.Users, tempUser)
	}
	return response, nil
}

func (r *UserRepo) UpdateUser(usr *pb.User) (*pb.User, error) {
	_, err := r.Db.Exec(`UPDATE users SET 
	first_name=$1, 
	last_name=$2,
	email=$3, 
	username=$4, 
	password=$5
	where id=$6`,
		usr.FirstName,
		usr.LastName,
		usr.Email,
		usr.Usenrame,
		usr.Password,
		usr.Id)
	return usr, err
}

func (r *UserRepo) DeletUser(ids *pb.Ids) error {
	for _, id := range ids.Ids {
		_, err := r.Db.Exec(`DELETE FROM users WHERE id = $1`, id)
		if err != nil {
			return err
		}
	}
	return nil
}

func (r *UserRepo) BuyProduct(req *pb.PutProductToDB) error {
	_, err := r.Db.Exec(`insert into user_properties(
		name, 
		category_name, 
		type_name, 
		amount, 
		price, 
		owner_id)
		values($1, $2, $3, $4, $5, $6)
		`,
		req.Name,
		req.CategoryName,
		req.TypeName,
		req.Amount,
		req.Price,
		req.OwnerId,
	)
	return err
}

func (r *UserRepo) GetProperty(OwnerId int64) ([]*pb.PutProductToDB, error) {
	rows, err := r.Db.Query(`SELECT 
	name, 
	category_name, 
	type_name, 
	amount, 
	price, 
	owner_id
	from user_properties where owner_id=$1
	`, OwnerId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	response := []*pb.PutProductToDB{}
	for rows.Next() {
		product := &pb.PutProductToDB{}
		err = rows.Scan(
			&product.Name,
			&product.CategoryName,
			&product.TypeName,
			&product.Amount,
			&product.Price,
			&product.OwnerId,
		)
		if err != nil {
			return nil, err
		}
		response = append(response, product)
	}
	return response, nil
}

func (r *UserRepo) SetProfileImage(req *pb.SetProfileImageReq) (*pb.GetProfileImageRes, error) {
	response := &pb.GetProfileImageRes{
		TypeInfo: req.TypeInfo,
		Int64S:   req.Int64S,
		OwnerId:  req.OwnerId,
	}
	err := r.Db.QueryRow(`insert into images(
		base64_s,
		type_info,
		owner_id
	) values($1, $2, $3) returning id`, req.Int64S, req.TypeInfo, req.OwnerId).Scan(&response.Id)

	return response, err
}

func (r *UserRepo) CheckField(req *pb.CheckFieldReq) (*pb.CheckFieldRes, error) {
	query := fmt.Sprintf("SELECT 1 FROM users WHERE %s=$1", req.Field)
	res := &pb.CheckFieldRes{}
	temp := -1
	err := r.Db.QueryRow(query, req.Value).Scan(&temp)
	if err != nil {
		res.Exist = false	
		return res, nil
	}
	if temp == 0 {
		res.Exist = true
	} else {
		res.Exist = false
	}
	return res, nil
}
