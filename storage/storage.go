package storage

import (
	"gitlab.com/grpc-first/user_service/storage/postgres"
	"gitlab.com/grpc-first/user_service/storage/repo"

	"github.com/jmoiron/sqlx"
)

type IStorage interface {
	User() repo.UserStorageI
}

type StoragePg struct {
	Db       *sqlx.DB
	userRepo repo.UserStorageI
}

// NewStoragePg
func NewStoragePg(db *sqlx.DB) *StoragePg {
	return &StoragePg{
		Db:       db,
		userRepo: postgres.NewUserRepo(db),
	}
}

func (s StoragePg) User() repo.UserStorageI {
	return s.userRepo
}
