CREATE TABLE IF NOT EXISTS user_properties (
    name TEXT NOT NULL,
    category_name TEXT,
    type_name TEXT,
    amount DECIMAL(10, 2) NOT NULL CHECK (amount >= 0),
    price DECIMAL(10, 2) NOT NULL CHECK (price >= 0)
);