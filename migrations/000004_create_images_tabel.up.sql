CREATE TABLE images(
    id SERIAL PRIMARY KEY,
    base64_s TEXT NOT NULL,
    type_info TEXT NOT NULL
);