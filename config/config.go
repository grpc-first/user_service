package config

import (
	"os"

	"github.com/spf13/cast"
)

type Config struct {
	Environment        string
	PostgresHost       string
	PostgresPort       int
	PostgresDatabase   string
	PostgresUser       string
	PostgresPassword   string
	LogLevel           string
	RPCPort            string
	PaymentServiceHost string
	PaymentServicePort int
	StoreServiceHost   string
	StoreServicePort   int
	ProductServiceHost string
	ProductServicePort int
}

// Load loads environment variables and inflates Config
func Load() Config {
	c := Config{}
	c.Environment = cast.ToString(GetOrReturnDefault("ENVIRONMENT", "develop"))

	c.PostgresHost = cast.ToString(GetOrReturnDefault("POSTGRES_HOST", "localhost"))
	c.PostgresPort = cast.ToInt(GetOrReturnDefault("POSTGRES_PORT", 5432))
	c.PostgresDatabase = cast.ToString(GetOrReturnDefault("POSTGRES_DATABASE", "user_service"))
	c.PostgresUser = cast.ToString(GetOrReturnDefault("POSTGRES_USER", "azizbek"))
	c.PostgresPassword = cast.ToString(GetOrReturnDefault("POSTGRES_PASSWORD", "Azizbek"))

	c.StoreServiceHost = cast.ToString(GetOrReturnDefault("S_S_HOST", "localhost"))
	c.StoreServicePort = cast.ToInt(GetOrReturnDefault("S_S_PORT", "6000"))

	c.PaymentServiceHost = cast.ToString(GetOrReturnDefault("P_S_HOST", "localhost"))
	c.PaymentServicePort = cast.ToInt(GetOrReturnDefault("P_S_PORT", "7000"))

	c.ProductServiceHost = cast.ToString(GetOrReturnDefault("PRODUCT_SERVICE_HOST", "localhost"))
	c.ProductServicePort = cast.ToInt(GetOrReturnDefault("PRODUCT_SERVICE_PORT", 9000))

	c.LogLevel = cast.ToString(GetOrReturnDefault("LOG_LEVEL", "debug"))

	c.RPCPort = cast.ToString(GetOrReturnDefault("RPC_PORT", ":8000"))
	return c
}

func GetOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}
	return defaultValue
}
