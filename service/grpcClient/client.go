package grpcclient

import (
	"fmt"

	cnf "gitlab.com/grpc-first/user_service/config"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	payS "gitlab.com/grpc-first/user_service/genproto/payment"
	ps "gitlab.com/grpc-first/user_service/genproto/product"
	ss "gitlab.com/grpc-first/user_service/genproto/store"
)

type ServiceManager struct {
	config         cnf.Config
	storeService   ss.StoreServiceClient
	productService ps.ProductServiceClient
	paymentService payS.PaymentServiceClient
}

func New(c cnf.Config) (*ServiceManager, error) {
	store, err := grpc.Dial(
		fmt.Sprintf("%s:%d", c.StoreServiceHost, c.StoreServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("error while dial Store service: host: %s and port: %d",
			c.StoreServiceHost, c.StoreServicePort)
	}
	product, err := grpc.Dial(
		fmt.Sprintf("%s:%d", c.ProductServiceHost, c.ProductServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("error while dial product service: host: %s and port: %d",
			c.ProductServiceHost, c.ProductServicePort)
	}

	pay, err := grpc.Dial(
		fmt.Sprintf("%s:%d", c.PaymentServiceHost, c.PaymentServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("error while dial payment service: host: %s and port: %d",
			c.PaymentServiceHost, c.PaymentServicePort)
	}
	serviceMen := &ServiceManager{
		config:       c,
		storeService: ss.NewStoreServiceClient(store),
		productService: ps.NewProductServiceClient(product),
		paymentService: payS.NewPaymentServiceClient(pay),
	}
	return serviceMen, nil
}

func (s *ServiceManager) Store() ss.StoreServiceClient {
	return s.storeService
}

func (s *ServiceManager) Product() ps.ProductServiceClient {
	return s.productService
}

func (s *ServiceManager) Payment() payS.PaymentServiceClient {
	return s.paymentService
}