package service

import (
	"context"

	pb "gitlab.com/grpc-first/user_service/genproto/user"
	"gitlab.com/grpc-first/user_service/pkg/logger"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (u *UserService) CheckField(ctx context.Context, req *pb.CheckFieldReq) (*pb.CheckFieldRes, error) {
	res, err := u.Storage.User().CheckField(req)
	if err != nil {
		u.Logger.Error("Error while checking field", logger.Any("Select", err))
		return nil, status.Error(codes.Internal, "Please check your data")
	}
	return res, nil
}
