package service

import (
	"context"

	"gitlab.com/grpc-first/user_service/pkg/logger"
	grpcclient "gitlab.com/grpc-first/user_service/service/grpcClient"
	"gitlab.com/grpc-first/user_service/storage"

	"gitlab.com/grpc-first/user_service/genproto/payment"
	"gitlab.com/grpc-first/user_service/genproto/store"
	pb "gitlab.com/grpc-first/user_service/genproto/user"

	"github.com/jmoiron/sqlx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type UserService struct {
	Client  *grpcclient.ServiceManager
	Storage storage.IStorage
	Logger  logger.Logger
}

func NewUserService(db *sqlx.DB, l logger.Logger, client *grpcclient.ServiceManager) *UserService {
	return &UserService{
		Client:  client,
		Storage: storage.NewStoragePg(db),
		Logger:  l,
	}
}

func (s *UserService) CreateUser(ctx context.Context, req *pb.User) (*pb.User, error) {
	res, err := s.Storage.User().Create(req)
	if err != nil {
		s.Logger.Error("Error while inserting into users", logger.Any("Insert", err))
		return &pb.User{}, status.Error(codes.Internal, "Something went wrong please check user info")
	}
	return res, nil
}

func (s *UserService) GetUsersById(ctx context.Context, in *pb.Ids) (*pb.GetUsers, error) {
	res, err := s.Storage.User().GetUsersById(in)
	if err != nil {
		s.Logger.Error("ERROR WHILE GETTING USERS", logger.Any("get", err))
		return &pb.GetUsers{}, status.Error(codes.NotFound, "Some of the users you ask are not exist")
	}
	return res, nil
}

func (s *UserService) UpdateUser(ctx context.Context, in *pb.User) (*pb.User, error) {
	res, err := s.Storage.User().UpdateUser(in)
	if err != nil {
		s.Logger.Error("Error while updating", logger.Any("Update", err))
		return &pb.User{}, status.Error(codes.InvalidArgument, "Please recheck user info")
	}
	return res, nil
}

func (s *UserService) DeleteUser(ctx context.Context, in *pb.Ids) (*pb.Empty, error) {
	err := s.Storage.User().DeletUser(in)
	if err != nil {
		s.Logger.Error("Error while deleting", logger.Any("Delete", err))
		return &pb.Empty{}, status.Error(codes.InvalidArgument, "Some of the users that you are going to delete are not exist")
	}
	return &pb.Empty{}, nil
}

func (s *UserService) BuyProduct(ctx context.Context, req *pb.BuyProductRequest) (*pb.BuyProductResponse, error) {
	buyReq := &store.SellProductRequest{
		CustomerId: req.CustomerId,
		ProductId:  req.ProductIds,
		Amount:     req.Amount,
		StoreId:    req.StoreId,
	}
	res, err := s.Client.Store().SellProductFromStore(ctx, buyReq)
	if err != nil {
		s.Logger.Error("Error while Buying", logger.Any("Buy", err))
		return nil, status.Error(codes.InvalidArgument, "Something went wrong")
	}

	dbReq := &pb.PutProductToDB{
		Name:         res.Product.Name,
		CategoryName: res.Product.Category.Name,
		TypeName:     res.Product.Type.Name,
		Amount:       req.Amount,
		Price:        res.TotalPrice / req.Amount,
	}
	dbReq.OwnerId = req.CustomerId
	err = s.Storage.User().BuyProduct(dbReq)
	if err != nil {
		s.Logger.Error("Error while Buying: s.Storage.User().BuyProduct(dbReq)", logger.Any("Buy", err))
		return nil, status.Error(codes.InvalidArgument, "Something went wrong")
	}
	response := &pb.BuyProductResponse{
		Store: &pb.StoreInfo{
			Id:      res.Store.Id,
			Name:    res.Store.Name,
			Address: (*pb.Address)(res.Store.Address),
		},
		Product: &pb.ProductInfo{
			Id:       res.Product.Id,
			Name:     res.Product.Name,
			Model:    res.Product.Model,
			Amount:   res.Product.Amount,
			Price:    res.Product.Price,
			Category: (*pb.Category)(res.Product.Category),
			Type:     (*pb.Type)(res.Product.Type),
		},
		TotalPrice:  res.TotalPrice,
		ProducCount: res.ProducCount,
		IsPaid:      res.IsPaid,
	}
	return response, nil
}

func (s *UserService) GetUserInfoById(ctx context.Context, req *pb.Id) (*pb.UserInfo, error) {
	user, err := s.Storage.User().GetUsersById(&pb.Ids{Ids: []int64{req.Id}})
	if err != nil || len(user.Users) != 1 {
		s.Logger.Error("s.Storage.User().GetUsersById(&pb.Ids{Ids: []int64{req.Id}})", logger.Any("get", err))
		return nil, status.Error(codes.NotFound, "Not Found")
	}
	u := user.Users[0]
	response := &pb.UserInfo{}
	response.Id = u.Id
	response.FirstName = u.FirstName
	response.LastName = u.LastName
	response.Email = u.Email
	response.Password = u.Password
	response.Username = u.Usenrame

	card, err := s.Client.Payment().GetCardInfo(ctx, &payment.Id{OwnerId: response.Id})
	if err != nil || len(user.Users) != 1 {
		s.Logger.Error("s.Client.Payment().GetCardInfo(ctx, &payment.Id{OwnerId: response.Id})", logger.Any("get", err))
		return nil, status.Error(codes.NotFound, "Not Found")
	}
	response.Card = (*pb.Card)(card)

	response.Products, err = s.Storage.User().GetProperty(req.Id)
	if err != nil || len(user.Users) != 1 {
		s.Logger.Error("s.Storage.User().GetProperty(req.Id)", logger.Any("get", err))
		return nil, status.Error(codes.NotFound, "Not Found")
	}
	return response, nil
}

func (s *UserService) SetProfileImage(ctx context.Context, req *pb.SetProfileImageReq) (*pb.GetProfileImageRes, error) {
	response, err := s.Storage.User().SetProfileImage(req)
	if err != nil {
		s.Logger.Error("Couldn't set profile image", logger.Any("create", err))
		return nil, status.Error(codes.Internal, "Check your data")
	}
	return response, nil
}
