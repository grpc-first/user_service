package main

import (
	"net"

	"gitlab.com/grpc-first/user_service/config"
	pb "gitlab.com/grpc-first/user_service/genproto/user"
	"gitlab.com/grpc-first/user_service/pkg/db"
	"gitlab.com/grpc-first/user_service/pkg/logger"
	"gitlab.com/grpc-first/user_service/service"
	grpcclient "gitlab.com/grpc-first/user_service/service/grpcClient"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	cfg := config.Load()

	log := logger.New(cfg.LogLevel, "user-service")
	defer logger.CleanUp(log)

	log.Info("main: sqlxConfig",
		logger.String("host", cfg.PostgresHost),
		logger.Int("port", cfg.PostgresPort),
		logger.String("database", cfg.PostgresDatabase))

	connDB, err := db.ConnectToDb(cfg)
	if err != nil {
		log.Fatal("Error while connecting to database", logger.Error(err))
	}

	serMeneger, err := grpcclient.New(cfg)
	if err != nil {
		log.Fatal("Error while connecting to database", logger.Error(err))
		return
	}
	userService := service.NewUserService(connDB, log, serMeneger)

	lis, err := net.Listen("tcp", cfg.RPCPort)
	if err != nil {
		log.Fatal("Error while listening 1: %v", logger.Error(err))
	}

	c := grpc.NewServer()
	reflection.Register(c)
	pb.RegisterUserServiceServer(c, userService)
	log.Info("Server is running",
		logger.String("port", cfg.RPCPort))

	if err := c.Serve(lis); err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}
}
